# coding:utf-8
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import MySQLdb
from config import config_database



def get_connection():
  return MySQLdb.connect(host=config_database['host'], user=config_database['user'], passwd=config_database['passwd'], db=config_database['db'],
                         charset=config_database['charset'])


def get_cursor(conn):
  return conn.cursor()
