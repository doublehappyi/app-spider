# coding:utf8
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import Queue
import auth
from bs4 import BeautifulSoup
import codecs
import json

#####################日志设置#####################
import utils
import os

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)

#####################日志设置#####################

from MysqlWorker import MysqlWorker


def getAppKeywordsQueue(url, headers, thread_num, index, result):
  q = Queue.Queue()
  html = utils.get_html(url, headers)

  bs = BeautifulSoup(html, 'html.parser')
  script = bs.select_one('#keyword-list > script')
  logger.warning(script)
  data = json.loads(script.get_text().split('=')[1])

  for item in data:
    if len(item) < 4:
      return
    keyword, indexNum, resultNum = (item[0], int(item[2]), int(item[3]))
    if indexNum >= index and resultNum <= result:
      q.put((keyword, indexNum, resultNum))

  for i in range(thread_num):
    q.put((None, None, None))
  return q


if __name__ == '__main__':
  logger.debug('Spider Start !')
  mysql_thread_num = 1
  # 获取cookie
  logger.debug('get cookie start ... ')
  cookie = auth.get_cookie()
  logger.debug('get cookie success: %s' % cookie)

  argv_len = len(sys.argv)
  if argv_len == 4:
    url, index, result = sys.argv[1], int(sys.argv[2]), int(sys.argv[3])
  elif argv_len == 2:
    # 设置搜索指数为4000， 搜索结果数为2000
    url, index, result = sys.argv[1], 4000, 1000
  else:
    # 用于测试
    url, index, result = 'http://www.guoaso.com/keyword?appid=1111665247&country=us&device=iphone', 4000, 1000

  logger.debug('url: ' + str(url))
  logger.debug('index: ' + str(index))
  logger.debug('result: ' + str(result))

  headers = {
    'Cookie': cookie
  }

  # 根据cookie和链接获取关键词数据
  resultQueue = getAppKeywordsQueue(url, headers, mysql_thread_num, index, result)

  mysql_worker = MysqlWorker(c_queue=resultQueue)

  mysql_worker.join()

  logger.warning('Spider Finished !')
