# coding:utf8
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import threading
import database

#####################日志设置#####################
import utils
import os

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


#####################日志设置#####################


class MysqlWorker(threading.Thread):
  def __init__(self, c_queue):
    super(MysqlWorker, self).__init__()
    self.c_queue = c_queue
    self.setDaemon(True)
    self.start()

  def run(self):
    conn = database.get_connection()
    cursor = database.get_cursor(conn)
    table_name = "keyword"
    while 1:
      try:
        keyword, name, result = self.c_queue.get()
        print name
        if keyword is not None:
          select_str = "select id from {0} where keyword=%s".format(table_name)
          if cursor.execute(select_str, (keyword,)):
            logger.debug('keyword already exists: {0}'.format(keyword))
          else:
            logger.debug('insert keyword: {0}'.format(keyword))
            insert_str = "insert into {0} values (NULL, %s, %s, %s)".format(table_name)
            cursor.execute(insert_str, (keyword, name, result))
            conn.commit()
        else:
          logger.debug('MysqlWorker: no more data in c_queue')
          break
      except Exception, e:
        logger.debug('MysqlWorker1: Exception %s' % (e))
      finally:
        self.c_queue.task_done()

    cursor.close()
    conn.close()
