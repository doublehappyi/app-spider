# coding:utf8
from config import config_user
import requests


# 登录,获取cookie
def get_cookie():
  data = {
    "username": config_user['username'],
    "password": config_user['password'],
    "forward": "http://aso114.com/account-myApp.html"
  }
  r = requests.post('http://aso114.com/account-signin.html', data=data)

  '''

Set-Cookie: FcXd_auth=0c753WPzspy2MvElJWctK2XLVZvhdmZlQKJc55en-KDhdz3m9XCbjnUSxo_KrXCW_ie3pvwtDc3Kubm3LtG-OKR4ljfZsrfPoVdWXrBgPOmLb78RfWZpnSW_X8R5eLqSu-Tj6WdBDCJLR-UrwXBR1hzVNmuicpnu; expires=Sat, 15-Oct-2016 12:46:55 GMT; path=/; domain=.aso114.com
Set-Cookie: FcXd__userid=ac2c-vlBySwQvqcEN1tdAsMhxsoc1MYo5Py07ZlFk_Qjmqg; expires=Sat, 15-Oct-2016 12:46:55 GMT; path=/; domain=.aso114.com
Set-Cookie: FcXd__username=6447WBGRWM9Ui9WgRCrqxzAkDmkeBOhOnsWiwtrx_ED7l6bZmG3JJA; expires=Sat, 15-Oct-2016 12:46:55 GMT; path=/; domain=.aso114.com
Set-Cookie: FcXd__groupid=1e187-_AhsE7AxSi4gmReUugpQgPWattlLzamTPf; expires=Sat, 15-Oct-2016 12:46:55 GMT; path=/; domain=.aso114.com
Set-Cookie: FcXd__email=a7afx1Jxr9jCuOwfExtHK1hqKZVWOEyV5ZcBMWfqi8pUD03wxVNgXgj73x3CCg; expires=Sat, 15-Oct-2016 12:46:55 GMT; path=/; domain=.aso114.com


  '''


  SetCookie = r.headers.get('Set-Cookie')

  print SetCookie
  FcXd_auth = SetCookie.split(';')[0]
  FcXd__userid = SetCookie.split('domain=.aso114.com,')[1].split(';')[0]

  # print FcXd_auth, FcXd__userid
  return ';'.join([FcXd_auth, FcXd__userid])

def get_headers():
  return {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate, sdch",
    "Accept-Language": "zh-CN, zh; q = 0.8, en; q = 0.6, zh-TW; q = 0.4, fr; q = 0.2, pt; q = 0.2",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive",
    "Cookie": get_cookie(),
    "DNT": "1",
    "Host": "aso114.com",
    "Pragma": "no-cache",
    "Referer": "http://aso114.com/account-signin.html",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36"

  }


if __name__ == '__main__':
  get_cookie()