# coding:utf8
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import threading
import urllib2
import json
from bs4 import BeautifulSoup
import re

#####################日志设置#####################
import utils
import os

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


#####################日志设置#####################


class OtherAppSpiderManager(object):
  def __init__(self, headers, c_queue, p_queue, c_thread_num=1, p_thread_num=1):
    self.headers = headers
    self.c_queue = c_queue
    self.p_queue = p_queue
    self.p_thread_num = p_thread_num
    self.threads = []

    self.init_threads(c_thread_num)

  def init_threads(self, c_thread_num):
    for i in xrange(c_thread_num):
      self.threads.append(OtherAppSpiderWorker(self.headers, self.c_queue, self.p_queue))

  def join(self):
    for t in self.threads:
      if t.isAlive():
        t.join()
    for i in range(self.p_thread_num):
      logger.debug('OtherAppSpiderManager:Putting (None, None, None, None, None) into p_queue')
      self.p_queue.put((None, None, None, None, None, None, None, None, None))


class OtherAppSpiderWorker(threading.Thread):
  def __init__(self, headers, c_queue, p_queue):
    super(OtherAppSpiderWorker, self).__init__()
    self.headers = headers
    self.c_queue = c_queue
    self.p_queue = p_queue
    self.setDaemon(True)
    self.start()

  def run(self):
    headers = self.headers
    while 1:
      try:
        appId = self.c_queue.get()
        if appId is None:
          break
        cover_data = self.get_app_data(appId, headers)
        self.p_queue.put(cover_data)
      except Exception, e:
        logger.warning('OtherAppSpiderWorker: Exception %s ' % (e))
      finally:
        self.c_queue.task_done()

  def get_app_data(self, appId, headers):
    url = 'http://aso114.com/app-keyword.html?appid='+appId
    data = []
    # reTemplate = re.compile(r"\<template .*?\<\/template\>", re.S)
    html = utils.get_html(url, headers=headers)
    # html = re.sub(reTemplate, '', html)

    bs = BeautifulSoup(html, 'html.parser')

    # 添加appid
    # data.append(bs.select('ul li')[1].select_one('p').get_text().strip())
    data.append(appId)
    # 添加app名字
    data.append(bs.select_one('.title').get_text().strip())

    # 添加价格
    data.append(bs.select('ul li')[3].select_one('p').get_text().strip())

    # 添加作者名字
    data.append(bs.select('ul li')[0].select_one('div > a > p').get_text().strip())
    # 添加app更新时间
    data.append('2016-01-01 00:00:00')
    # 添加cover数据: cover_all, cover_top3, cover_top10
    tds = bs.select('.p_list_count > .row')[0].select('a.m_tip_text')

    for td in tds:
      t = td.get_text().strip().split("总数为")[1].split("个")[0]
      if t == '':
        t = 0
      data.append(int(t))
    return tuple(data)
