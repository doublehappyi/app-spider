DROP DATABASE IF EXISTS aso;
###数据库
CREATE DATABASE IF NOT EXISTS aso DEFAULT CHARSET utf8 ;
use aso;


###地区表
CREATE TABLE IF NOT EXISTS `cover`(
  `id` INT unsigned NOT NULL auto_increment PRIMARY KEY,
  `appid` INT NOT NULL,
  `appname` VARCHAR(255) NOT NULL,
  `price` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `update_time` DATE NOT NULL,
  `cover_all` INT NOT NULL ,
  `cover_top3` INT NOT NULL ,
--   `cover_top5` INT NOT NULL ,
  `cover_top10` INT NOT NULL ,
  `updated_at` DATETIME NOT NULL,
  `created_at` DATETIME NOT null
) ENGINE=innodb DEFAULT CHARSET=utf8 ;

