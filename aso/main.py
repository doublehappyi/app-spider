# coding:utf8
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import Queue
import auth
from bs4 import BeautifulSoup
import re

#####################日志设置#####################
import utils
import os

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)

#####################日志设置#####################

from OtherAppSpider import OtherAppSpiderManager
from MysqlWorker import MysqlWorker


def getMyAppIds(headers):
  appIds = []
  url = 'http://aso114.com/account-myApp.html'
  html = utils.get_html(url, headers)

  bs = BeautifulSoup(html, 'html.parser')
  items = bs.select('.media-heading > a')

  for item in items:
    appid = item.get('href').split('appid=')[1]
    logger.debug('appid: %s' % appid)

    appIds.append(appid)

  return appIds


def getOtherAppIdsQueue(headers, appIds, next_thread_num):
  q = Queue.Queue()
  for appId in appIds:
    maxPage = getMaxPage(appId, headers)
    for page in range(1, maxPage + 1):
      url = 'http://aso114.com/app-samepubapp.html?appid=' + appId + '&page=' + str(page)
      html = utils.get_html(url, headers=headers)
      # logger.debug(html)
      bs = BeautifulSoup(html, 'html.parser')
      items = bs.select('.list > a')
      for item in items:
        currAppId = item.get('href').split('appid=')[1]
        q.put(currAppId)

  for i in range(next_thread_num):
    q.put(None)
  return q


def getMaxPage(appId, headers):
  url = 'http://aso114.com/app-samepubapp.html?appid='+appId
  reTemplate = re.compile(r"\<template .*?\<\/template\>", re.S)
  html = utils.get_html(url, headers=headers)
  html = re.sub(reTemplate, '', html)

  # logger.debug(html)

  bs = BeautifulSoup(html, 'html.parser')
  a_list = bs.select('.m_page > span > a')
  a_list_len = len(a_list)
  if a_list_len > 2:
    max_page = int(a_list[len(a_list) - 2].get('href').split('page=')[1])
  else:
    max_page = 1

  return max_page


if __name__ == '__main__':
  logger.debug('Spider Start !')
  other_app_spider_thread_num = 15
  mysql_thread_num = 1
  # 获取cookie
  logger.debug('get cookie start ... ')
  cookie = auth.get_cookie()
  logger.debug('get cookie sucess: %s' % cookie)
  headers = auth.get_headers()
  # 根据cookie获取我关注的appid
  logger.debug('get appids start ...')
  appIds = getMyAppIds(headers)
  logger.debug('get appids success')
  # 根据我关注的appid,获取对应appid所属用户的所有appid队列
  otherAppIdsQueue = getOtherAppIdsQueue(headers, appIds, other_app_spider_thread_num)

  resultQueue = Queue.Queue()

  other_app_spider_mgr = OtherAppSpiderManager(headers=headers, c_queue=otherAppIdsQueue, p_queue=resultQueue,
                                               c_thread_num=other_app_spider_thread_num, p_thread_num=mysql_thread_num)
  mysql_worker = MysqlWorker(c_queue=resultQueue)

  other_app_spider_mgr.join()
  mysql_worker.join()

  logger.warning('Spider Finished !')
