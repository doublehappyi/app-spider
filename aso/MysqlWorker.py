# coding:utf8
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import threading
import database

#####################日志设置#####################
import utils
import os

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


#####################日志设置#####################


class MysqlWorker(threading.Thread):
  def __init__(self, c_queue):
    super(MysqlWorker, self).__init__()
    self.c_queue = c_queue
    self.setDaemon(True)
    self.start()

  def run(self):
    conn = database.get_connection()
    cursor = database.get_cursor(conn)
    table_name = "cover"
    while 1:
      try:
        appid, appname, price, author, update_time, cover_all, cover_top3, cover_top5, cover_top10 = self.c_queue.get()
        if appid is not None:
          # print appid, appname, cover_all, cover_top3, cover_top5, cover_top10
          select_str = "select id from {0} where appid=%s and updated_at > %s".format(table_name)
          today = utils.today()
          now = utils.now()

          # 如果当天已经有数据了,就更新数据;否则新增数据
          if cursor.execute(select_str, (appid, today)):
            logger.debug('update appid: {0}'.format(appid))
            # update data
            update_str = "update {0} as t set " \
                         "t.price=%s, t.author=%s, t.update_time=%s, t.cover_all=%s, t.cover_top3=%s, " \
                         "t.cover_top10=%s, t.updated_at=%s " \
                         "where appid=%s and created_at > %s".format(table_name)
            cursor.execute(update_str, (price, author, update_time, cover_all, cover_top3, cover_top10, now, appid, today))
            conn.commit()
          else:
            logger.debug('insert appid: {0}'.format(appid))
            # insert data
            insert_str = "insert into {0} values (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)".format(table_name)
            cursor.execute(insert_str, (appid, appname, price, author, update_time, cover_all, cover_top3, cover_top10, now, now))
            conn.commit()
        else:
          logger.debug('MysqlWorker: no more data in c_queue')
          break
      except Exception, e:
        logger.debug('MysqlWorker: Exception %s' % (e))
      finally:
        self.c_queue.task_done()

    cursor.close()
    conn.close()
