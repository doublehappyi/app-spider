# coding:utf8
from config import config_user
import requests


# 登录,获取cookie
def get_cookie():
  data = {
    "username": config_user['username'],
    "password": config_user['password'],
    "remember": "true"
  }
  r = requests.post('http://guoaso.com/loginform', data=data)

  SetCookie = r.headers.get('Set-Cookie')
  MemberID = SetCookie.split(';')[0]
  MemberIDMD5 = SetCookie.split('path=/, ')[1].split(';')[0]

  return ';'.join([MemberID, MemberIDMD5])

def get_headers():
  return {
    'Cookie': get_cookie()
  }
