# coding:utf8
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import threading
import urllib2
import json
from bs4 import BeautifulSoup

#####################日志设置#####################
import utils
import os

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


#####################日志设置#####################


class OtherAppSpiderManager(object):
  def __init__(self, headers, c_queue, p_queue, c_thread_num=1, p_thread_num=1):
    self.headers = headers
    self.c_queue = c_queue
    self.p_queue = p_queue
    self.p_thread_num = p_thread_num
    self.threads = []

    self.init_threads(c_thread_num)

  def init_threads(self, c_thread_num):
    for i in xrange(c_thread_num):
      self.threads.append(OtherAppSpiderWorker(self.headers, self.c_queue, self.p_queue))

  def join(self):
    for t in self.threads:
      if t.isAlive():
        t.join()
    for i in range(self.p_thread_num):
      logger.debug('OtherAppSpiderManager:Putting (None, None, None, None, None) into p_queue')
      self.p_queue.put((None, None, None, None, None, None, None, None))


class OtherAppSpiderWorker(threading.Thread):
  def __init__(self, headers, c_queue, p_queue):
    super(OtherAppSpiderWorker, self).__init__()
    self.headers = headers
    self.c_queue = c_queue
    self.p_queue = p_queue
    self.setDaemon(True)
    self.start()

  def run(self):
    headers = self.headers
    while 1:
      try:
        appId = self.c_queue.get()
        if appId is None:
          break
        cover_data = self.get_app_data(appId, headers)
        self.p_queue.put(cover_data)
      except Exception, e:
        logger.warning('OtherAppSpiderWorker: Exception %s , appId: %s' % (e, appId))
      finally:
        self.c_queue.task_done()

  def get_app_data(self, appId, headers):
    url = 'http://guoaso.com/keyword?appid=' + appId + '&country=cn&device=iphone'
    data = []
    defaultData = (appId, 'failed', 0, 'failed', '2016-01-01', 0, 0, 0)
    try:
      html = utils.get_html(url, headers=headers)
      if html is None:
        logger.warning('get html failed, appid: %s' % appId)
        return defaultData
      bs = BeautifulSoup(html, 'html.parser')

      #添加appid
      data.append(appId)
      # 添加app名字
      data.append(bs.select('.appinfo-title')[0].get("title"))

      contentItems = bs.select('.appinfo-body > .appinfo-info .content')
      #添加价格
      data.append(contentItems[3].get_text())
      # 添加作者名字
      data.append(contentItems[0].get_text())
      #添加app更新时间
      data.append(contentItems[4].get_text())
      #添加cover数据: cover_all, cover_top3, cover_top10
      tds = bs.select('.apprankchange-country')[0].select('td')

      for td in tds:
        data.append(td.get_text())
      return tuple(data)
    except Exception as e:
      logger.warning('OtherAppSpiderWorker: get_app_data: Exception %s , appId: %s, html %s' % (e, appId, html))
      return defaultData
