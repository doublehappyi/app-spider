# coding:utf8
__author__ = 'db'
import sys

reload(sys)
sys.setdefaultencoding('utf8')
import urllib
import codecs
import Queue
import auth
from bs4 import BeautifulSoup

#####################日志设置#####################
import utils
import os

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)

#####################日志设置#####################

from OtherAppSpider import OtherAppSpiderManager
from MysqlWorker import MysqlWorker


def getMyAppIds(headers):
  appIds = []
  url = 'http://guoaso.com/myapp'
  html = utils.get_html(url, headers)

  bs = BeautifulSoup(html, 'html.parser')
  items = bs.select('.media-heading > a')

  for item in items:
    appid = item.get('href').split('appid=')[1]
    logger.debug('appid: %s' % appid)

    appIds.append(appid)

  return appIds


def getOtherAppIdsQueue(appIds, next_thread_num):
  q = Queue.Queue()
  headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate, sdch",
    "Accept-Language": "zh-CN, zh; q = 0.8, en; q = 0.6, zh-TW; q = 0.4, fr; q = 0.2, pt; q = 0.2",
    "Cache-Control": "no-cache",
    "Connection": "keep-alive",
    "Cookie": "",
    "DNT": "1",
    "Host": "guoaso.com",
    "Pragma": "no-cache",
    "Referer": "http://guoaso.com/apprank?appid=113908623",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36"
  }
  for appId in appIds:
    url = 'http://guoaso.com/samepubapp?appid=' + str(appId) + '&country=cn&device=iphone'

    html = utils.get_html(url, headers=headers)

    bs = BeautifulSoup(html, 'html.parser')
    items = bs.select('.thumbnail > a')
    for item in items:
      currAppId = item.get('href').split('appid=')[1]
      q.put(currAppId)

  for i in range(next_thread_num):
    q.put(None)
  return q


if __name__ == '__main__':
  logger.debug('Spider Start !')
  other_app_spider_thread_num = 15
  mysql_thread_num = 1
  # 获取cookie
  logger.debug('get cookie start ... ')
  cookie = auth.get_cookie()
  logger.debug('get cookie success: %s' % cookie)
  headers = {
    'Cookie': cookie
  }
  # 根据cookie获取我关注的appid
  logger.debug('get appids start ...')
  appIds = getMyAppIds(headers)
  logger.debug('get appids success')
  # 根据我关注的appid,获取对应appid所属用户的所有appid队列
  logger.debug('getOtherAppIdsQueue start')
  otherAppIdsQueue = getOtherAppIdsQueue(appIds, other_app_spider_thread_num)
  logger.debug('getOtherAppIdsQueue success')
  resultQueue = Queue.Queue()

  other_app_spider_mgr = OtherAppSpiderManager(headers=headers, c_queue=otherAppIdsQueue, p_queue=resultQueue,
                                               c_thread_num=other_app_spider_thread_num, p_thread_num=mysql_thread_num)
  mysql_worker = MysqlWorker(c_queue=resultQueue)

  other_app_spider_mgr.join()
  mysql_worker.join()

  logger.warning('Spider Finished !')
