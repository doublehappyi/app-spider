# coding=utf8

import requests
from bs4 import BeautifulSoup
import database
from config import config_user

# ####################日志设置#####################
import os, utils

log_path = os.path.dirname(os.path.abspath(__file__)) + '/log'
logger = utils.get_logger(log_path)


# ####################日志设置#####################

#登录,获取cookie
def get_cookie():
  data = {
    "username": config_user['username'],
    "password": config_user['password'],
    "remember": "true"
  }
  r = requests.post('http://guoaso.com/loginform', data=data)

  SetCookie = r.headers.get('Set-Cookie')
  MemberID = SetCookie.split(';')[0]
  MemberIDMD5 = SetCookie.split('path=/, ')[1].split(';')[0]

  return ';'.join([MemberID, MemberIDMD5])

#根据html获取appids列表
def get_appids(html):
  soup = BeautifulSoup(html, 'html.parser')

  items = soup.select('.main > a')
  appids = []

  for item in items:
    appids.append(item.get('href').split('appid=')[1])

  return appids

#根据url和cookie, 获取目标数据: cover_all, cover_top3, cover_top10
def get_data(url, headers):
  data = []
  html = utils.get_html(url, headers=headers)

  bs = BeautifulSoup(html, 'html.parser')
  
  #添加app名字
  data.append(bs.select('.appinfo-title')[0].get_text())
  tds = bs.select('.apprankchange-country')[0].select('td')

  for td in tds:
    data.append(td.get_text())
  return data


if __name__ == '__main__':
  url_apps = 'http://www.guoaso.com/member/settingitc'
  cookie = get_cookie()
  logger.debug('cookie: {0}'.format(cookie))
  headers = {
    'Cookie': cookie
  }
  html_apps = utils.get_html(url_apps, headers=headers)
  appids = get_appids(html_apps)

  conn = database.get_connection()
  cursor = database.get_cursor(conn)
  table_name = "cover"

  for appid in appids:
    url = 'http://guoaso.com/keyword?appid=' + appid + '&country=cn&device=iphone'
    appname, cover_all, cover_top3, cover_top10 = get_data(url, headers=headers)
    select_str = "select id from {0} where appid=%s and updated_at > %s".format(table_name)
    today = utils.today()
    now = utils.now()

    #如果当天已经有数据了,就更新数据;否则新增数据
    if cursor.execute(select_str, (appid, today)):
      logger.debug('update appid: {0}'.format(appid))
      # update data
      update_str = "update {0} as t set t.cover_all=%s, t.cover_top3=%s, cover_top10=%s, updated_at=%s where appid=%s and created_at > %s".format(
        table_name)
      cursor.execute(update_str, (cover_all, cover_top3, cover_top10, now, appid, today))
      conn.commit()
    else:
      logger.debug('insert appid: {0}'.format(appid))
      # insert data
      insert_str = "insert into {0} values (NULL, %s, %s, %s, %s, %s, %s, %s)".format(table_name)
      cursor.execute(insert_str, (appid, appname, cover_all, cover_top3, cover_top10, now, now))
      conn.commit()

  cursor.close()
  conn.close()

  logger.debug('guoaso spider over!')
