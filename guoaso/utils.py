# coding:utf-8
import os, logging, time
import requests
import datetime

def get_logger(log_path):
  log_file_name = time.strftime("%Y%m%d-%H-%M", time.localtime())
  log_format = "%(asctime)s: %(levelname)s: %(message)s"
  if not os.path.exists(log_path):
    os.mkdir(log_path)

  #如果需要修改debug级别, 修改level参数即可
  logging.basicConfig(filename=log_path + '/' + log_file_name + '.log', filemode='w', level=logging.WARNING,
                      format=log_format)
  logger = logging.getLogger(__file__)
  ch = logging.StreamHandler()
  ch.setLevel(logging.WARNING)
  formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
  ch.setFormatter(formatter)
  logger.addHandler(ch)

  return logger


def get_html(url, headers):
  try:
    r = requests.get(url, headers=headers)
    r.encoding = 'utf-8'
    html = r.text
  except Exception as e:
    html = None
  return html


def now():
  return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

def today():
  return str(datetime.date.today().strftime('%Y-%m-%d')) + " 00:00:00"
