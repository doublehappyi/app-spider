DROP DATABASE IF EXISTS guoaso;
###数据库
CREATE DATABASE IF NOT EXISTS guoaso
  DEFAULT CHARSET utf8;
USE guoaso;

###地区表
CREATE TABLE IF NOT EXISTS `cover` (
  `id`          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `appid`       INT          NOT NULL,
  `appname`     VARCHAR(255) NOT NULL,
  `cover_all`   INT          NOT NULL,
  `cover_top3`  INT          NOT NULL,
  `cover_top10` INT          NOT NULL,
  `updated_at`  DATETIME     NOT NULL,
  `created_at`  DATETIME     NOT NULL
)
  ENGINE = innodb
  DEFAULT CHARSET = utf8;

ALTER TABLE `cover`
  ADD COLUMN `price` VARCHAR(64) NOT NULL
  AFTER `appname`;
ALTER TABLE `cover`
  ADD COLUMN `author` VARCHAR(64) NOT NULL
  AFTER `price`;
ALTER TABLE `cover`
  ADD COLUMN `update_time` DATE NOT NULL
  AFTER `author`;


CREATE TABLE IF NOT EXISTS `keyword` (
  `id`      INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `keyword` VARCHAR(64)  NOT NULL,
  `name`   INT UNSIGNED NOT NULL,
  `result`  INT UNSIGNED NOT NULL,
  UNIQUE KEY (`keyword`)
)
  ENGINE = innodb
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `sanmao_statistic` (
  `id`    INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type`  INT UNIQUE   NOT NULL, #页面访问统计：1； 点击统计：2
  `count` INT          NOT NULL
);
insert into sanmao_statistic VALUES (1, 1, 0), (2, 2, 0);
